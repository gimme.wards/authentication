﻿using Authencation;

public class User
{
    public readonly UserCredential UserCredential;

    public User(string login,string password)
    {
        UserCredential = new(login,password);
    }

    public User(UserCredential userCredential)
    {
        UserCredential = userCredential;
    }
}