﻿namespace Authencation;

public struct ValidationResult
{
    public readonly bool IsSuccess;
    public readonly string? ErrorMessage = null;

    private ValidationResult(bool isSuccess, string? errorMessage)
    {
        IsSuccess = isSuccess;
        ErrorMessage = errorMessage;
    }

    public static ValidationResult Ok
        => new ValidationResult(true, null);

    public static ValidationResult Error(string message)
        => new ValidationResult(false, message);
}