﻿using System.Collections.Specialized;

namespace Authencation;

public class PasswordValidator:IValidator
{
    // Добавить условия, должен содержать хотя бы одну букву, цифру и хотя бы 1 символ не являющийся буквой. И в разных регистрах
    public ValidationResult Validate(string password)
    {
        return password.Length >= 5
               && password.Any(char.IsDigit)
               && password.Any(char.IsLetter)
               && password.Any(char.IsUpper)
               && password.Any(char.IsLower)
               && password.Any(char.IsLetter)
               && password.Any(c => !char.IsLetter(c) && !char.IsDigit(c))
            ? ValidationResult.Ok
            : ValidationResult.Error("Неверный пароль");
    }
}