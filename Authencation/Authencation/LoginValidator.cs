﻿namespace Authencation;

public class LoginValidator : IValidator
{
    // Добавить проверку что логин состоит только из латинских букв и цифр. Первый символ обязательно буква.
    // У char есть проверка для цифр.

    public ValidationResult Validate(string login)
    {
        return login.Length > 3  
               && char.IsLetter(login[0])
               && login.All(symbol=> char.IsDigit(symbol) || char.IsLetter(symbol))
            ? ValidationResult.Ok
            : ValidationResult.Error("Длинна Login должна быть не меньше 3 символов");
    }
}