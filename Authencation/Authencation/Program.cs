﻿namespace Authencation;

public class Program
{
    static IoProvider ioProvider = new();
    public static void Main()
    {
        
        Menu menu = new Menu(ioProvider,("Зарегистрироваться",SignUp),("Войти",Login));
        menu.ShowSelectAndExecute();
      
    }

    private static void Login()
    {
        UserCredential userCredential = ioProvider.GetUserCredential();
        using var reader = File.OpenText("users.txt");

        while (!reader.EndOfStream)
        {
            var text = reader.ReadLine();
            UserCredential newUser = text == null ? throw new ArgumentException() : UserCredential.Parse(text);
            if (userCredential.Equals(newUser))
            {
                Console.WriteLine("Входим");
                return;
            }
        }
        throw new ArgumentException();
    }

    private static void SignUp()
    {
        Console.WriteLine("Делаем регистрацию");
        UserCredential userCredential=ioProvider.GetUserCredential();
        User user = new User(userCredential);
        ioProvider.WelcomeMessege(user);
        using var writer = File.AppendText("users.txt");
        writer.WriteLine(userCredential);
    }
}