namespace Authencation;

public class MenuItemNumberValidator: IValidator
{
    private readonly int _itemMaxNumber;

    public MenuItemNumberValidator(int itemMaxNumber)
    {
        _itemMaxNumber = itemMaxNumber;
    }
    
    public ValidationResult Validate(string value)
    {
        return int.TryParse(value, out int num) && num >= 0 && num < _itemMaxNumber
            ? ValidationResult.Ok
            : ValidationResult.Error("Введенное значение должно быть числом.");
    }
}