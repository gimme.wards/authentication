namespace Authencation;

public class Menu
{
    private List<IMenuItem> _items = new ();
    private IoProvider _ioProvider;

    public Menu(IoProvider ioProvider,params (string text, Action action)[] menuItemTexts)
    {
        _items.AddRange(menuItemTexts.Select(item=> new TextMenuItem(item.text,item.action)));
        _ioProvider = ioProvider;
    }
    public void ShowSelectAndExecute()
    {
        foreach (var item in _items)
        {
            item.Show();
        }
        int itemNumber = _ioProvider.GetNumber("номер выбранного пункта меню", _items.Count);
        _items[itemNumber].Run();
    }
    
    interface IMenuItem
    {
        void Show();
        void Run();
    }
    class TextMenuItem:IMenuItem
    {
        private readonly string _text;
        private readonly Action _executor;

        public TextMenuItem(string text, Action executor)
        {
            _text = text;
            _executor = executor;
        }
        public void Show()
        {
            Console.WriteLine(_text);
        }
        public void Run()
        {
            _executor();
        }
    }

}