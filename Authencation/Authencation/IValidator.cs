﻿namespace Authencation;

public interface IValidator
{
    ValidationResult Validate(string value);
}