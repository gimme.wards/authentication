﻿namespace Authencation;

public struct UserCredential
{
    public static readonly IValidator LoginValidator =new LoginValidator();
    public static readonly IValidator PasswordValidator = new PasswordValidator();
    public readonly string Login;
    private readonly string _password;

    public UserCredential(string login , string password)
    {
        if (!LoginValidator.Validate(login).IsSuccess)
            throw new ArgumentException(LoginValidator.Validate(login).ErrorMessage);
        if(!PasswordValidator.Validate(password).IsSuccess)
            throw new ArgumentException(LoginValidator.Validate(login).ErrorMessage);

        Login = login;
        _password = password;
    }

    public override string ToString()
    {
        return Login +";"+ _password;
    }

    public static UserCredential Parse(string textValue)
    {
        return new UserCredential(textValue.Split(";")[0], textValue.Split(";")[1]);
    }
}