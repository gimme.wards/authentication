﻿namespace Authencation;

public class IoProvider
{
    public UserCredential GetUserCredential()
    {
        string name = GetFromConsole("логин",UserCredential.LoginValidator);
        string password = GetFromConsole("пароль", UserCredential.PasswordValidator);
        return new UserCredential(name, password);
    }

    private string GetFromConsole(string valueName, IValidator validator)
    {
        Console.WriteLine($"Введите {valueName}");
        
        while (true)
        {
            string? input = Console.ReadLine();
            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Была введена пустая строка, попробуйте еще раз");
                continue;
            }
            ValidationResult result = validator.Validate(input);
            if (!result.IsSuccess)
            {
                Console.WriteLine(result.ErrorMessage);
                continue;
            }
            return input;
        }
    }

    public int GetNumber(string valueName, int itemMaxNumber)
    {
        string value = GetFromConsole(valueName, new MenuItemNumberValidator(itemMaxNumber));
        return int.Parse(value);
    }

    public void WelcomeMessege(User user)
    {
        Console.WriteLine($"Добро пожаловать {user.UserCredential.Login}");
    }
}